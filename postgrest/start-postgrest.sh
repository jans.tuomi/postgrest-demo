#!/bin/bash

# Insert env vars into postgrest.conf
envsubst < postgrest.conf > postgrest.conf.tmp
mv postgrest.conf.tmp postgrest.conf

./postgrest postgrest.conf
