-- authenticator role. created manually, see README.md
-- create role authenticator noinherit login password '${POSTGREST_PASSWORD}';

-- web_anon role
create role web_anon nologin;
grant usage on schema api to web_anon;
grant web_anon to authenticator;
-- grant select on api.todos to web_anon;

-- todo_user role
create role todo_user nologin;
grant usage on schema api to todo_user;
grant all on api.todos to todo_user;
grant usage, select on sequence api.todos_id_seq to todo_user;
grant todo_user to authenticator;
