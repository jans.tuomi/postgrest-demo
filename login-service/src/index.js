const express = require('express');
const axios = require('axios');
var jwt = require('jsonwebtoken');

const app = express();
app.use(express.json());
app.use((req, res, next) => {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Headers', '*');
  next();
});

const port = 4000;

const JWT_SECRET = process.env.JWT_SECRET;

const authorizeUserNamePassword = (username, password) => {
  // TODO
  const now = Math.round(new Date().getTime() / 1000);

  return {
    role: 'todo_user',
    exp: now + 30 * 60, // 30 minutes
  };
};

const authorizeGoogle = googleUser => {
  // TODO
  return {
    role: 'todo_user',
    exp: Number(googleUser.exp),
  };
};

app.get('/', (req, res) => res.send('Login service API'));

// Username + Password login
app.post('/login/userpass', (req, res) => {
  const payload = req.body;
  const { username, password } = payload;
  const claims = authorizeUserNamePassword(username, password);
  const signedJWT = jwt.sign(claims, JWT_SECRET);
  res.json({ jwt: signedJWT, displayName: username });
});

// Google access token login
app.post('/login/google', async (req, res) => {
  const payload = req.body;
  const { accessToken } = payload;
  try {
    const resp = await axios.get(`https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=${accessToken}`);
    const payload = resp.data;
    const { email } = payload;
    const claims = authorizeGoogle(payload);
    const signedJWT = jwt.sign(claims, JWT_SECRET);
    res.json({ jwt: signedJWT, displayName: email });
  } catch (err) {
    console.error(err);
  }
});

app.listen(port, () => console.log(`Listening at http://localhost:${port}`));
