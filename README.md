# PostgREST system demo

# Installation

* Run
```
$ docker-compose build
```

* Create file `.env` with all variables required in `docker-compose.yml`.
* Set up `authenticator` user role in the database by running

```
$ docker-compose run --rm postgrest_api bash -c "
  PGPASSWORD=postgres psql -h postgres -U postgres -c \"\
    create role authenticator noinherit login password '${POSTGREST_PASSWORD}'
  \"
"
```

# Running migrations

Migration files live under `postgrest/migrations`. Run them all with
```
$ docker-compose run --rm postgrest_api bash -c '
for sqlfile in migrations/*.sql
do
  echo Migrating ${sqlfile}...
  PGPASSWORD=postgres psql -U postgres -h postgres < ${sqlfile}
done'
```

# Launching services

* Run
```
$ docker-compose up
```

Copyright Jan Tuomi 2020